import numpy as np
import pandas as pd
import os
import matplotlib.pyplot as plt
import seaborn as sns
import warnings
from sklearn.compose import ColumnTransformer
from sklearn.pipeline import Pipeline
from sklearn.preprocessing import StandardScaler
from sklearn.metrics import r2_score,mean_absolute_error
from sklearn.linear_model import LinearRegression
from sklearn.neighbors import KNeighborsRegressor
from sklearn.tree import DecisionTreeRegressor
from sklearn.ensemble import RandomForestRegressor
from sklearn.svm import SVR
from xgboost import XGBRegressor
from sklearn.model_selection import train_test_split

warnings.filterwarnings('ignore')

def model_validation(model):
    step1 = ColumnTransformer(transformers=[
    ('col_tnf',StandardScaler(), [0,1,2,3,4,5,6,7,8,9,10])], remainder='passthrough')
    step2 = model
    pipe = pipe = Pipeline([
    ('step1',step1),
    ('step2',step2)])
    pipe.fit(X_train,y_train)
    y_pred = pipe.predict(X_test)
    return(float(r2_score(y_test,y_pred)), float(mean_absolute_error(y_test,y_pred)))

df = pd.read_csv('../data/phones.csv')

df.drop('Product_id', inplace = True, axis = 1)
df.drop('Sale', inplace = True, axis = 1)

X = df.drop('Price', axis = 1)
y = df['Price']

X_train,X_test,y_train,y_test = train_test_split(X,y,test_size=0.15,random_state=106)

regressors = ['LinearRegression', 'KNeighborsRegressor', 'DecisionTreeRegressor', 'RandomForestRegressor', 'SVR', 'XGBRegressor']

rscore = []
mae = []
models = [LinearRegression(),
          KNeighborsRegressor(n_neighbors=3),
          DecisionTreeRegressor(max_depth=8),
          RandomForestRegressor(),
          SVR(kernel='linear'),
          XGBRegressor()]
for model in models:
    r,m = model_validation(model)
    rscore.append(r)
    mae.append(m)

data = {'Regression':regressors, 'R squared':rscore,'MAE':mae}
result = pd.DataFrame(data)

result