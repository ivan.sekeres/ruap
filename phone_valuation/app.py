from flask import Flask, render_template, request, jsonify
import json
import os
import ssl
import urllib.request

def allowSelfSignedHttps(allowed):
    if allowed and not os.environ.get('PYTHONHTTPSVERIFY', '') and getattr(ssl, '_create_unverified_context', None):
        ssl._create_default_https_context = ssl._create_unverified_context

allowSelfSignedHttps(True)

app = Flask(__name__, static_url_path='/static')

@app.route('/')
def index():
    return render_template('index.html')

@app.route('/submit_form', methods=['POST'])
def submit_form():
    data_from_form = request.json

    azure_endpoint_url = 'https://projekt-ruap.westeurope.inference.ml.azure.com/score'

    headers = {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer VZxMy3Q4rKptIYfCavQCB4OF1La34qHD',
        'azureml-model-deployment': 'projektruap'
    }

    body = json.dumps({"input_data": data_from_form}).encode()

    req = urllib.request.Request(azure_endpoint_url, body, headers)
    
    try:
        response = urllib.request.urlopen(req)
        result = response.read()
        return result[1:8]
        
    except urllib.error.HTTPError as error:
        print("The request failed with status code: " + str(error.code))
        print(error.info())
        print(error.read().decode("utf8", 'ignore'))
        return jsonify({'message': f'Error code {error.code}'})

if __name__ == '__main__':
    app.run(debug=True)
